package com.tui.proof.ws.service;

import com.tui.proof.configuration.OrderConfig;
import com.tui.proof.entity.OrderEntity;
import com.tui.proof.exception.OrderLockedException;
import com.tui.proof.mapper.ClientEntityMapper;
import com.tui.proof.mapper.OrderEntityMapper;
import com.tui.proof.model.OrderType;
import com.tui.proof.model.dto.Address;
import com.tui.proof.model.dto.Client;
import com.tui.proof.model.dto.Order;
import com.tui.proof.ws.notifier.Notifier;
import com.tui.proof.ws.repository.ClientEntityRepository;
import com.tui.proof.ws.repository.OrderEntityRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

  @InjectMocks
  OrderService orderService;

  @Mock
  OrderEntityRepository repository;

  @Mock
  OrderEntityMapper orderTransformer;

  @Mock
  OrderConfig orderConfig;

  @Mock
  @SuppressWarnings("unused")
  ClientEntityRepository clientRepository;

  @Mock
  @SuppressWarnings("unused")
  ClientEntityMapper clientTransformer;

  @Mock
  @SuppressWarnings("unused")
  Notifier notifier;

  @Test
  @DisplayName("Testing find all the orders")
  void findAll() {

    OrderEntity orderEntity1 = new OrderEntity(
            1L,
            5,
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            null,
            null);
    OrderEntity orderEntity2 = new OrderEntity(
            2L,
            10,
            BigDecimal.valueOf(25),
            LocalDateTime.now(),
            null,
            null);

    Order order1 = new Order(
            1L,
            OrderType.P5,
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            null,
            null);
    Order order2 = new Order(
            2L,
            OrderType.P10,
            BigDecimal.valueOf(25),
            LocalDateTime.now(),
            null,
            null);

    List<OrderEntity> mockOrderEntityList = new ArrayList<>(2);
    mockOrderEntityList.add(orderEntity1);
    mockOrderEntityList.add(orderEntity2);

    List<Order> mockOrderList = new ArrayList<>(2);
    mockOrderList.add(order1);
    mockOrderList.add(order2);

    when(repository.findByOptionalClientData(null, null, null, null)).thenReturn(mockOrderEntityList);
    when(orderTransformer.orderEntityListToOrderList(mockOrderEntityList)).thenReturn(mockOrderList);

    List<Order> orderList = orderService.findAll(null, null, null, null);

    assertEquals(2, orderList.size());
    verify(repository, times(1)).findByOptionalClientData(null, null, null, null);
    verify(orderTransformer, times(1)).orderEntityListToOrderList(mockOrderEntityList);

  }

  @Test
  @DisplayName("Testing find by id")
  void getById() {

    OrderEntity mockOrderEntity = new OrderEntity(
            1L,
            5,
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            null,
            null);

    Order mockOrder = new Order(
            1L,
            OrderType.P5,
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            null,
            null);

    when(repository.findById(1L)).thenReturn(Optional.of(mockOrderEntity));
    when(orderTransformer.orderEntityToOrder(mockOrderEntity)).thenReturn(mockOrder);

    Order order = orderService.findById(1L);

    assertEquals(1L, order.getNumber());
    verify(repository, times(1)).findById(1L);
    verify(orderTransformer, times(1)).orderEntityToOrder(mockOrderEntity);
  }

  @Test
  @DisplayName("Testing the new order creation")
  void newOrder() {

    BigDecimal unitPrice = BigDecimal.valueOf(5);
    OrderType orderType = OrderType.P5;
    BigDecimal expectedTotalPrice = unitPrice.multiply(BigDecimal.valueOf(orderType.toInteger()));

    OrderEntity mockOrderEntity = new OrderEntity(
            1L,
            5,
            BigDecimal.valueOf(30),
            LocalDateTime.now(),
            null,
            null);

    Client client = new Client("James", "Smith", "600111111", "jamessmith@mail.com");
    Address address = new Address("mock street", "901234", "Malaga", "Spain");
    Order mockOrder = new Order(1L, orderType, null, null, address, client);

    when(orderConfig.getPilotesPrice()).thenReturn(unitPrice);
    when(repository.save(mockOrderEntity)).thenReturn(mockOrderEntity);
    when(orderTransformer.orderEntityToOrder(mockOrderEntity)).thenReturn(mockOrder);
    when(orderTransformer.orderToOrderEntity(mockOrder))
            .thenAnswer(
                    mock -> {
                      Order mockedOrder = mock.getArgument(0);
                      assertNotNull(mockedOrder.getOrderDate());
                      assertNotNull(mockedOrder.getTotalPrice());
                      return mockOrderEntity;
                    });

    Order order = orderService.newOrder(mockOrder);

    assertEquals(1L, order.getNumber());
    assertEquals(BigDecimal.valueOf(25), order.getTotalPrice());
    assertEquals(expectedTotalPrice, order.getTotalPrice());
    assertNotNull(order.getOrderDate());
    verify(repository, times(1)).save(mockOrderEntity);
  }

  @Test
  @DisplayName("Testing the order lock")
  void orderLocked() {
    int minutesToLock = 5;

    OrderEntity mockOrderEntity = new OrderEntity(
            1L,
            5,
            BigDecimal.valueOf(30),
            LocalDateTime.now().minusMinutes(minutesToLock).minusMinutes(1),
            null,
            null);

    when(orderConfig.getMinutesToLock()).thenReturn(minutesToLock);
    when(repository.findById(1L)).thenReturn(Optional.of(mockOrderEntity));

    Order order = new Order(null, null, null, null, null, null);

    assertThrows(
            OrderLockedException.class,
            () -> orderService.putOrder(order, 1L));
  }

}

package com.tui.proof.ws.service;

import com.tui.proof.configuration.OrderConfig;
import com.tui.proof.entity.OrderEntity;
import com.tui.proof.exception.OrderLockedException;
import com.tui.proof.exception.OrderNotFoundException;
import com.tui.proof.mapper.ClientEntityMapper;
import com.tui.proof.mapper.OrderEntityMapper;
import com.tui.proof.model.dto.Order;
import com.tui.proof.ws.notifier.Notifier;
import com.tui.proof.ws.repository.ClientEntityRepository;
import com.tui.proof.ws.repository.OrderEntityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Log4j2
@RequiredArgsConstructor
@Service
public class OrderService {

  private final OrderEntityRepository orderRepository;
  private final OrderEntityMapper orderEntityMapper;
  private final ClientEntityRepository clientEntityRepository;
  private final ClientEntityMapper clientEntityMapper;
  private final Notifier notifier;
  private final OrderConfig orderConfig;

  public List<Order> findAll(String firstName, String lastName, String email, String telephone) {

    List<OrderEntity> orderEntityList;
    orderEntityList = orderRepository.findByOptionalClientData(firstName, lastName, email, telephone);
    return orderEntityMapper.orderEntityListToOrderList(orderEntityList);
  }

  public Order findById(Long id) throws OrderNotFoundException {
    return orderEntityMapper.orderEntityToOrder(
            orderRepository
                    .findById(id)
                    .orElseThrow(() -> new OrderNotFoundException(id)));
  }

  public Order newOrder(Order order) {

    return processOrder(order, false);
  }

  public Order putOrder(Order order, Long id) throws OrderLockedException {

    OrderEntity orderEntity = orderRepository.findById(id).orElse(null);
    boolean exists = orderEntity != null;

    if (orderLocked(orderEntity)) {
      throw new OrderLockedException(id);
    }

    order.setNumber(id);
    return processOrder(order, exists);
  }

  public void deleteOrder(Long id) throws OrderLockedException {
    OrderEntity orderEntity = orderRepository
            .findById(id)
            .orElseThrow(() -> new OrderNotFoundException(id));

    if (orderLocked(orderEntity)) {
      throw new OrderLockedException(id);
    } else {
      orderRepository.deleteById(id);
    }
  }

  /**
   * After 5 minutes from the order time, the order can't be modified
   *
   * @param orderEntity the order to modify
   * @return if the order is locked (can't be modified)
   */
  private boolean orderLocked(OrderEntity orderEntity) {

    if (orderEntity == null) {
      return false;
    }

    return (LocalDateTime.now().isAfter(
            orderEntity.getOrderDate().plusMinutes(orderConfig.getMinutesToLock())));
  }

  /**
   * Calculates some fields of the order
   *
   * @param order the order to prepare
   */
  private void prepareOrder(Order order) {
    order.setOrderDate(LocalDateTime.now());
    order.setTotalPrice(orderConfig.getPilotesPrice().multiply(BigDecimal.valueOf(order.getPilotes().toInteger())));
  }

  /**
   * Checks the order information, saves it and notifies.
   *
   * @param order the order to be processed
   * @return the saved order
   */
  private Order processOrder(Order order, Boolean update) {
    prepareOrder(order);

    clientEntityRepository.save(clientEntityMapper.clientToClientEntity(order.getClient()));

    Order savedOrder = orderEntityMapper.orderEntityToOrder(
            orderRepository.save(orderEntityMapper.orderToOrderEntity(order)));

    notifier.notifyOrder(savedOrder, update);

    return savedOrder;
  }

}

package com.tui.proof.ws.controller;

import com.tui.proof.model.dto.Order;
import com.tui.proof.ws.service.OrderService;
import com.tui.proof.ws.assembler.OrderModelAssembler;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.groups.Default;
import java.util.List;
import java.util.stream.Collectors;

@Log4j2
@Validated
@AllArgsConstructor
@RestController
public class OrderController {

  private final OrderService orderService;

  private final OrderModelAssembler assembler;

  @GetMapping("/orders")
  public ResponseEntity<CollectionModel<EntityModel<Order>>> findAll(
          @RequestParam(required = false) String firstName,
          @RequestParam(required = false) String lastName,
          @RequestParam(required = false) String email,
          @RequestParam(required = false) String telephone) {

    List<EntityModel<Order>> orderList = orderService.findAll(firstName, lastName, email, telephone).stream()
            .map(assembler::toModel).collect(Collectors.toList());

    CollectionModel<EntityModel<Order>> collectionModel = CollectionModel.of(orderList, WebMvcLinkBuilder
            .linkTo(WebMvcLinkBuilder
            .methodOn(OrderController.class).findAll(firstName, lastName, email, telephone))
            .withSelfRel()
            .expand());

    return ResponseEntity.ok(collectionModel);
  }

  @PostMapping("/orders")
  @Validated(Default.class)
  public ResponseEntity<EntityModel<Order>> newOrder(@Valid @RequestBody Order order){
    EntityModel<Order> entityModel = assembler.toModel(orderService.newOrder(order));
    return ResponseEntity
            .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
  }

  @GetMapping("/orders/{id}")
  public ResponseEntity<EntityModel<Order>> getById(@PathVariable Long id){
    EntityModel<Order> entityModel =  assembler.toModel(orderService.findById(id));
    return ResponseEntity.ok(entityModel);
  }

  @PutMapping("/orders/{id}")
  @Validated(Default.class)
  public ResponseEntity<EntityModel<Order>> putOrder(@Valid @RequestBody Order order, @PathVariable Long id){
    EntityModel<Order> entityModel =  assembler.toModel(orderService.putOrder(order, id));
    return ResponseEntity
            .created(entityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
            .body(entityModel);
  }

  @DeleteMapping("/orders/{id}")
  public ResponseEntity<Void> deleteOrder(@PathVariable Long id){
    orderService.deleteOrder(id);
    return ResponseEntity.noContent().build();
  }

}

package com.tui.proof.ws.repository;

import com.tui.proof.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderEntityRepository extends JpaRepository<OrderEntity, Long> {

  @Query("SELECT "
          + "DISTINCT (orderEntity) FROM OrderEntity orderEntity "
          + "JOIN FETCH "
          + "orderEntity.client client "
          + "WHERE "
          + "(UPPER(client.firstName) LIKE UPPER(CONCAT('%', :firstName, '%')) OR :firstName IS NULL) AND"
          + "(UPPER(client.lastName) LIKE UPPER(CONCAT('%', :lastName, '%')) OR :lastName IS NULL) AND"
          + "(UPPER(client.email) LIKE UPPER(CONCAT('%', :email, '%')) OR :email IS NULL) AND"
          + "(UPPER(client.telephone) LIKE UPPER(CONCAT('%', :telephone, '%')) OR :telephone IS NULL)")
  List<OrderEntity> findByOptionalClientData(
          @Param("firstName") String firstName,
          @Param("lastName") String lastName,
          @Param("email") String email,
          @Param("telephone") String telephone);

}

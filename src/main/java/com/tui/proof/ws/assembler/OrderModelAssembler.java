package com.tui.proof.ws.assembler;

import com.tui.proof.model.dto.Order;
import com.tui.proof.ws.controller.OrderController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.stereotype.Component;

@Component
public class OrderModelAssembler implements RepresentationModelAssembler<Order, EntityModel<Order>> {

  @Override
  public EntityModel<Order> toModel(Order order) {

    return EntityModel.of(order,
            WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder
                    .methodOn(OrderController.class).getById(order.getNumber()))
                    .withSelfRel(),
            WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder
                    .methodOn(OrderController.class).findAll(null, null, null, null))
                    .withRel("orders")
                    .expand());
  }
}

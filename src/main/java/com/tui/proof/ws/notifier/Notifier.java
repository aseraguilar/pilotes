package com.tui.proof.ws.notifier;

import com.tui.proof.model.dto.Order;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Log4j2
@Component
public class Notifier {

  /**
   * Notifies Miquel about a new order or about an order modification
   * @param order the order
   * @param update if true, the order already exists, is a modification. Else, is a new order.
   */
  @Async
  public void notifyOrder(Order order, boolean update) {

    if (order != null) {
      if (update) {
        log.info(String.format("Miquel! The order %s has been modified. You must cook %s pilotes",
                order.getNumber(), order.getPilotes().toInteger()));
      } else {
        log.info(String.format("Miquel! The order %s has arrived. You must cook %s pilotes",
                order.getNumber(), order.getPilotes().toInteger()));
      }
    }
  }

}

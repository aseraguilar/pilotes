package com.tui.proof.ws.security;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    //TODO: The access level for each role whould be setted here. On descent order of restriction(i.e. ADMIN, USER...)
    http
      .csrf().disable()
      .authorizeRequests().antMatchers(HttpMethod.GET, "/orders").authenticated()
      .and()
      .httpBasic();
  }

  /* TODO:
   * Spring default user is set on application.properties. On a real API the users and its roles could be
   * stored on a database. Always with encrypted passwords.
   */

}

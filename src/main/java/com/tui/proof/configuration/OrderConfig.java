package com.tui.proof.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;

@Data
@Configuration
public class OrderConfig {

  private final Integer minutesToLock;
  private final BigDecimal pilotesPrice;

  public OrderConfig(
          @Value("${order.modification.minutesToLock}")
                  Integer minutesToLock,
          @Value("${order.pilotes.price}")
                  BigDecimal pilotesPrice) {
    this.minutesToLock = minutesToLock;
    this.pilotesPrice = pilotesPrice;
  }

}

package com.tui.proof.exception;

public class OrderLockedException extends RuntimeException{

  public OrderLockedException(Long id) {
    super(String.format("The order %s can not be modified, Miquel started cooking the pilotes.", id));
  }

}

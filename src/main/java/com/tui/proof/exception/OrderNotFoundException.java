package com.tui.proof.exception;

public class OrderNotFoundException extends RuntimeException{

  public OrderNotFoundException(Long id) {
    super(String.format("Could not find the order with number %s", id));
  }

}

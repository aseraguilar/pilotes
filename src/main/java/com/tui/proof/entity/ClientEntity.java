package com.tui.proof.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class ClientEntity {

    @Id
    private String email;

    private String firstName;

    private String lastName;

    private String telephone;

}

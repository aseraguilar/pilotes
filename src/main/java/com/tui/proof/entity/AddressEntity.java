package com.tui.proof.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class AddressEntity {

  @Id
  @GeneratedValue
  private Long id;

  private String street;

  private String postcode;

  private String city;

  private String country;

}

package com.tui.proof.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
public class OrderEntity {

  @Id
  @GeneratedValue
  private Long number;

  private Integer pilotes;

  private BigDecimal totalPrice;

  private LocalDateTime orderDate;

  @ManyToOne(cascade = CascadeType.ALL)
  private AddressEntity deliveryAddress;

  @ManyToOne(cascade = CascadeType.MERGE)
  private ClientEntity client;
}

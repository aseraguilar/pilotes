package com.tui.proof.mapper;

import com.tui.proof.entity.ClientEntity;
import com.tui.proof.model.dto.Client;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClientEntityMapper {
  ClientEntity clientToClientEntity(Client client);
}

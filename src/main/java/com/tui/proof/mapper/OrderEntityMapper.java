package com.tui.proof.mapper;

import com.tui.proof.entity.OrderEntity;
import com.tui.proof.model.OrderType;
import com.tui.proof.model.dto.Order;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrderEntityMapper {

  @Mapping(source = "order.pilotes.", target = "pilotes")
  OrderEntity orderToOrderEntity(Order order);
  @Named("orderEntityToOrder")
  Order orderEntityToOrder(OrderEntity orderEntity);
  @IterableMapping(qualifiedByName = "orderEntityToOrder")
  List<Order> orderEntityListToOrderList(List<OrderEntity> orders);

  static Integer map(OrderType orderType) {
    return orderType.toInteger();
  }

  static OrderType map(Integer integer) {
    return OrderType.toOrderType(integer);
  }

}

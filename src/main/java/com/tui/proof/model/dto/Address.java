package com.tui.proof.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
public class Address {

  @NotBlank(message = "The street is mandatory")
  private String street;

  @NotBlank(message = "The postal code is mandatory")
  private String postcode;

  @NotBlank(message = "The city is mandatory")
  private String city;

  private String country;
}

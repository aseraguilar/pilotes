package com.tui.proof.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.tui.proof.model.OrderType;
import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class Order {

  private Long number;

  @NotNull(message = "The number of polites is mandatory")
  private OrderType pilotes;

  private BigDecimal totalPrice;

  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss")
  private LocalDateTime orderDate;

  @NotNull(message = "The delivery address is mandatory")
  @Valid
  private Address deliveryAddress;

  @NotNull(message = "The client is mandatory")
  @Valid
  private Client client;

}

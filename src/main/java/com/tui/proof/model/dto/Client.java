package com.tui.proof.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class Client {

  @NotBlank(message = "First name is mandatory")
  @Size(min=2, message = "Enter a valid first name")
  private String firstName;

  @Size(min=2, message = "Enter a valid last name")
  private String lastName;

  @Digits(fraction = 0,integer = 9, message = "The phone number it not valid")
  private String telephone;

  @Email(message = "The email is not valid")
  @NotBlank(message = "Email is mandatory")
  private String email;

}

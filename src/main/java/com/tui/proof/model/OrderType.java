package com.tui.proof.model;

public enum OrderType {

  P5(5), P10(10), P15(15);

  private final int value;

  OrderType(int value) {
    this.value = value;
  }

  public Integer toInteger() {
    return value;
  }

  public static OrderType toOrderType(Integer value) {
    for (OrderType orderType: OrderType.values()) {
      if (orderType.toInteger().equals(value)) {
        return orderType;
      }
    }

    return null;
  }

}

# PILOTES ORDERING

This is a REST API for ordering pilotes from **Miquel Montoro**.

**Author: Aser Aguilar**

# Resources

The available endpoints are:

- GET  `/orders`
- POST `/orders`
- GET  `/orders/{id}`
- PUT  `/orders/{id}`
- DELETE `/orders/{id}`

**All resources called with `GET` method require *Basic Auth:***  
*User: user*  
*Password: password*

# Entities

The *Mandatory / Optional* annotations are meant for the body requests.

>>Client
>- firstName: **Mandatory.** String greater than 2 characters. The first name of the client.
>- lastName: **Optional.** String. The last name of the client.
>- telephone: **Optional.** 9 digit with no spaces. The telephone of the client.
>- email: **Mandatory.** String. The email of the client.

>>Delivery Address
>- street: **Mandatory.** String. The street for the delivery.
>- postcode: **Mandatory.** String. The postal code for the delivery
>- city: **Mandatory.** String. The city for the delivery
>- country: **Optional.** String. The country for the delivery.

The number of pilotes has to be chosen between 5, 10 and 15, represented by the values P5, P10 and P15 respectively
>>Order
>- number: A unique number to identify the order.
>- pilotes: **Mandatory**. Enum.The number of pilotes to order.
>- client: **Mandatory**. A Client entity. The client who makes the order
>- deliveryAddress: **Mandatory**. A Delivery Address entity. The address where de deliver should be done
>- orderDate: The time when the order was done or modified.
>- totalPrice: The amount the client has to pay.

## Get a list of orders
This request allows the user to get a list of orders.
<pre>
[GET] /orders{?firstName,lastName,email,telephone}
</pre>

###Parameters
The endpoint allows the user to filter the order by some client fields.
- First name
- Last name
- Email
- Telephone

The filters are of type "contains" and not case-sensitive.  
Sample:
<pre>
[GET]  /orders?firstName=al
</pre>
This call would filter any client with "al" on its name like"Alan" or "Donald".

Response Sample
<pre>
{
    "_embedded": {
        "orderList": [
            {
                "number": 1,
                "pilotes": "P10",
                "totalPrice": 15.00,
                "orderDate": "2022-03-22 18:29:23",
                "deliveryAddress": {
                    "street": "test street",
                    "postcode": "90123",
                    "city": "Malaga",
                    "country": "Spain"
                },
                "client": {
                    "firstName": "Billy",
                    "lastName": null,
                    "telephone": "900123456",
                    "email": "billyTest@mail.com"
                },
                "_links": {
                    "self": {
                        "href": "http://localhost:8080/orders/1"
                    },
                    "orders": {
                        "href": "http://localhost:8080/orders"
                    }
                }
            }
        ]
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/orders"
        }
    }
}
</pre>

## Get a specific order
This request allows the user to get a specific order.
<pre>
[GET] /orders/{id}
</pre>

If the order does not exist the server will return a `404 Not Found` status response

Response Sample
<pre>
{
    "number": 1,
    "pilotes": "P10",
    "totalPrice": 15.00,
    "orderDate": "2022-03-22 00:32:01",
    "deliveryAddress": {
        "street": "test street",
        "postcode": "90123",
        "city": "Malaga",
        "country": "Spain"
    },
    "client": {
        "firstName": "Billy",
        "lastName": null,
        "telephone": "900123456",
        "email": "billyTest@mail.com"
    },
    "_links": {
        "self": {
            "href": "http://localhost:8080/orders/1"
        },
        "orders": {
            "href": "http://localhost:8080/orders"
        }
    }
}
</pre>

## Create a new order
This request allows the user make a new order
<pre>
[POST] /orders
</pre>

This endpoint requires to send the `Order` entity on the body with the mandatory fields.

Request body sample
<pre>
{
    "deliveryAddress": {
        "street": "delivery street",
        "postcode": "90123",
        "city": "Malaga",
        "country": "Spain"
    },
    "pilotes": "P10",
    "client": {
        "firstName": "Billy",
        "email": "billyTest@mail.com",
        "telephone": "900123456"
    }
}
</pre>

## Create or update an order
This request allows the user to update an existing order. If the specified id does not exist, a new order will be made.
<pre>
[PUT] /orders{id}
</pre>


**Warning:** After 5 minutes since the order was made, it can not be modified or deleted. The server will return a `409 Conflict` status response  

The request body is the same specified on `[POST] /orders`

## Delete an order
This request allows the user to delete an order.
<pre>
[DELETE] /orders{id}
</pre>

**Warning:** After 5 minutes since the order was made, it can not be modified or deleted.  

If the order does not exist the server will return a `404 Not Found` status response
